# Mule Sample Projects

## Goal

This repository contains MuleSoft sample projects (i.e.: tutorials, how-to's, use case implementations, PoC's, etc.)

## Projects

- [basic-auth-with-spring](https://bitbucket.org/leonard-alves/mule-samples/src/master/basic-auth-with-spring/): Sample project implementing Spring Authorization
- [https-api](https://bitbucket.org/leonard-alves/mule-samples/src/master/https-api/): Sample project implementing HTTPS in Mule 4.
- [logger-separation](https://bitbucket.org/leonard-alves/mule-samples/src/master/logger-separation/): Sample project implementing log4j separation based on the log levels
- [vm-message-dispatcher](https://bitbucket.org/leonard-alves/mule-samples/src/master/vm-message-dispatcher/): Sample project implementing message dispatcher pattern.